# Preamble-Project

## We the people believe that opportunity, inclusion, and hope for the future, establish a more perfect union; not legislation, medication or further incarceration.

## Qualitative Goals
1. Bridge the skills gap by delivering readily available resources to provide the __applied digital skills training__ into our American prison systems, county jails, and juvenile detention centers.  Into the hands of our socioeconomically disadvantaged; a grossly under, and unemployed subsector of the US economy.
2. Leverage the same plant that, to this day, continues to be unjustly used to incarcerate over 600,000 Americans per year, as means to gainful employment, opportunity, economic stability, and improved pscyhe of the formerly incarcerated.
3. In making the problem the solution, the expected outcome is:
    - reduction of the illicit marijuana market
    - boost to the 
        - Medicinal, 
        - Recreational, and 
        - Whole herbaceous plant; hemp plant sector. 
    - further gains to what had been uncollected tax revenues.
    
## Quantitative Goals
1. See Prohibition End
1. Realize 7 year exit strategy & conversion to not-for profit business model.
1. Help Draft Opioid Epidemic Resolution Strategy pivoting to full-time commitment on seeing that realized. 
1. Facilitate the creation of in-demand careers over the course of the next 7 years.
    - IT: 250
    - Cannabis: 250
    - Hemp Farm: 250

## Elements: 
The People Problems | The Plant Problems | The Profitability Possibilities
------|------|------| 
Growing Gaps | Psychoactive-free / Phytocannabinoid Rich | Cannabidiol Craze
Substance Use Disorder (SUD) | Medical Maijuana  | Grant Funding  
Opioid Crisis | Recreational | Okay Google Actions are needed
Recidivism & Reentry | Whole Plant Cannabacea | Industrial Hemp
Greed | Non-compliant Market | Ancillary Services
**SWOT** | **Call to Act** | **Summary**

### Non-Negotiables
1. Open source, open border, open for all as an always accessible, fully transparent repository on the github.com platform.
1.  All-inlcusive. Expect and respect diversity. A True Meritocracy.
1. Establish  and implement simple yet actionable measurables fueling data-driven Solutions.
1. Launch from Google Cloud Platform; full leverage of the platform and resources as to progressively deliver current and emerging technologies, utilize all that is machine learning, Artificial Intelligence, kubernetes, and Natural Language Processing. 
   a. Finally, as chief principal of Chasing the Wind, llc all benefits from the Google Cloud for Startups program we have been awarded are to utilized in their entirety as a means to kickstart the launch, facilitating lean funding.
## Will it work?
### [The Last Mile Project](https://thelastmile.org/), a framework and blueprint worthy of emulation, prepares incarcerated individuals for successful reentry through business and technology training. Using a four-pillar approach.
Their motto:
    > **Changing Lives Through Tech**

Education | Vocation | Expansion | Reentry
--------- | -------- | --------- | ---------
Simple | Attainable | Lean | Record of Success 
**Served 460 students since '10** | **12 locations Ca, In, Ks, Ok** | **0% recidivism rate** | **National Avg. 55%**



## The Ancillary Model 
Diagnostic | therapeutic | Custodial 
---------- | ----------- | --------
Prospecting | Training | From 1st Day Out
🩺 | **w/ Best in Class Pay** | 🔬
 
## People Problem No. 1
**IT and the Digital Skills Gap**
Cloud Services, AI, and Machine Learning  are developing at break-neck speeds while driving faster growth and efficiency for businesses and society but there is a lack of digital skills in the workforce.
